(ns plf03.core)


(defn función-comp-1
  []
  (let [calcularid1 (fn [datosuser] (apply str datosuser))
        calcularid2 (fn [datosuser] (concat datosuser))
        calcularid (comp calcularid1 calcularid2)]
    (calcularid ["Manuel" 22 1998])))

(defn función-comp-2
  []
  (let [solo221 (fn [datosuser] (filter (fn [x] (true? (> x 22))) datosuser))
        solo222 (fn [datosuser] (map - [2020] datosuser))
        calcularsemestre (comp solo222 solo221)]
    (calcularsemestre [1998 1998 1996 1995 2000])))

(defn función-comp-3
  []
  (let [soloing-1 (fn [datosuser] (filter (fn [x] (= x "ing")) datosuser))
        soloing-2 (fn [datosuser] (map (fn [x] (apply str x)) (map (fn [da] (take 3 da)) datosuser)))
        soloing-3 (fn [datosuser] (map (fn [datosuser] (take 3 datosuser))datosuser))
        soloing (comp soloing-1 soloing-2 soloing-3)]
    (soloing ["ing. Manuel" "ing. Victor" "lic. Moises"])))

(defn función-comp-4
  []
  (let [soloenteros-1 (fn [numero] (filter int? numero))
        soloenteros-2 (fn [numero] (map (fn [x] (/ 7 x)) numero))
        soloenteros (comp  soloenteros-1 soloenteros-2)]
    (soloenteros [1 2 3 4 5 6])))

(defn función-comp-5
  []
  (let [conv-1 (fn [lista] (map (fn [x] (* x -1)) lista))
        conv-2 (fn [lista] (map (fn [x] (- x 100)) lista))
        conv (comp conv-1 conv-2)]
    (conv [2 3 4])))

(defn función-comp-6
  []
  (let [ordenkey-1 (fn [listakeys] (sort listakeys))
        ordenkey-2 (fn [listakeys] (keys listakeys))
        ordenkey (comp ordenkey-1 ordenkey-2)]
    (ordenkey {:nombre "Manuel" :apellido "Salinas" :edad 22 :carrera "ISC"})))

(defn función-comp-7
  []
  (let [ordenval-1 (fn [listavalues] (sort listavalues))
        ordenval-2 (fn [listavalues] (vals listavalues))
        ordenkey (comp ordenval-1 ordenval-2)]
    (ordenkey {:numeroC 16920405 :id 12854 :matricula 774623})))

(defn función-comp-8
  []
  (let [vaciadecimal-1 (fn [lista] (empty? lista))
        vaciadecimal-2 (fn [lista] (filter int? (map (fn [x] (/ x 9)) lista)))
        vaciadecimal (comp vaciadecimal-1 vaciadecimal-2)]
    (vaciadecimal [1 2 34 56 18])))

(defn función-comp-9
  [] 
  (let [juntar-1 (fn [lista] (apply str lista))
        juntar-2 (fn [lista] (flatten lista))
        juntar (comp juntar-1 juntar-2)]
    (juntar [["Victor "] ["Manuel "] ["Vasquez "] ["Salinas "]])))

(defn función-comp-10
  []
  (let [borrarpositivos-1 (fn [lista] (remove pos? lista))
        borrarpositivos-2 (fn [lista] (filter number? lista))
        borrarpositivos (comp borrarpositivos-1 borrarpositivos-2)]
    (borrarpositivos ["Manuel" 2 -56 \s 1 4 "Salinas" -5 -7])))

(defn función-comp-11
  []
  (let [sortrev-1 (fn [lista] (reverse lista))
        sortrev-2 (fn [lista] (sort lista))
        sortrev (comp sortrev-1 sortrev-2)]
    (sortrev [99 45 32 78 3 6 8])))

(defn función-comp-12
  []
  (let [evenfun-1 (fn [lista] (keep even? lista))
        evenfun-2 (fn [lista] (range (first lista) (last lista)))
        evenfun (comp evenfun-1 evenfun-2)]
    (evenfun '(2 3 4 76 33 17))))

(defn función-comp-13
  []
  (let [sortdec-1 (fn [lista] (sort-by count lista))
        sortdec-2 (fn [lista] (map str lista))
        sortdec (comp sortdec-1 sortdec-2)]
    (sortdec [123 455 7789 5 434 5])))

(defn función-comp-14
  []
  (let [multi-1 (fn [lista] (take 3 lista))
        multi-2 (fn [lista] (range (first lista) (last lista)))
        multi (comp multi-1 multi-2)]
    (multi [1 54 32 78 10])))

(defn función-comp-15 
  []
  (let [doblecoll-1 (fn [lista] (mapv + lista lista))
        doblecoll-2 (fn [lista] (filter number? lista))
        doblecoll (comp doblecoll-1 doblecoll-2)]
    (doblecoll ["Manuel" \a 1 4 5 7 32 54])))

(defn función-comp-16 
  []
  (let [conmap-1 (fn [lista] (map dec lista))
        conmap-2 (fn [lista] (map inc lista))
        conmap (comp conmap-1 conmap-2)]
    (conmap [1 4 6 232 45 7878])))

(defn función-comp-17 
  []
  (let [agrup-1 (fn [lista] (group-by count lista))
        agrup-2 (fn [lista] (map str lista))
        agrup (comp agrup-1 agrup-2)]
    (agrup [1 45 67 43 \a "Manuel" "Salinas"])))

(defn función-comp-18
  []
  (let [filtr-1 (fn [lista] (filterv (fn [x] (= (count x) 3)) lista))
        filtr-2 (fn [lista] (map (fn [x] (take 3 x)) lista))
        filtr (comp filtr-1 filtr-2)]
    (filtr ["Manuel" "Vasquez" "Salinas" "Victor"])))

(defn función-comp-19
  []
  (let [borrpos-1 (fn [lista] (pos-int? lista))
        borrpos-2 (fn [lista] (get lista 0))
        borrpos (comp  borrpos-1 borrpos-2)]
    (borrpos [-1 2 45 677 4233 3])))

(defn función-comp-20
  []
  (let [sumdou-1 (fn [lista] (double? lista))
        sumdou-2 (fn [lista] (fn [x] (+ (get x 0) (get x 1))) lista)
        sumdou (comp sumdou-1 sumdou-2)]
    (sumdou [4 9.9 4 6.7])))


(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)


(defn función-complement-1
  []
  (let [invint-1 (fn [lista a] (pos? (get lista a)))
       invint (complement invint-1)]
    (invint [-1 2 4 5 6] 0)))

(defn función-complement-2 
  []
  (let [isvec-1 (fn [valo] (vector? (into [] valo)))
        isvec (complement isvec-1)]
    (isvec "Manuel Salinas")))

(defn función-complement-3
  []
  (let [hig-1 (fn [a b] (> a b))
        hig (complement hig-1)]
    (hig 3 5)))

(defn función-complement-4
  []
  (let [eq-1 (fn [a b] (= a b))
        eq (complement eq-1)]
    (eq 3 5)))

(defn función-complement-5
  []
  (let [boo-1 (fn [numero] (boolean? (pos? numero)))
        boo (complement boo-1)]
    (boo -2)))

(defn función-complement-6
  []
  (let [doub-1 (fn [a b] (double? (/ a b))) 
        doub (complement doub-1)]
    (doub 4 5.4)))

(defn función-complement-7
  []
  (let [numb-1 (fn [lista a] (number? (get lista a)))
        numb (complement numb-1)]
    (numb ["Manuel" 1 4 6 "Salinas"] 4)))

(defn función-complement-8
  []
  (let [seqq-1 (fn [lista] (seq? (filter string? lista)))
        seqq (complement seqq-1)]
    (seqq ["Manuel" 22 1998 "Salinas"])))

(defn función-complement-9
  []
  (let [som-1 (fn [lista] (some?(remove string? lista)))
        som (complement som-1)]
    (som ["Salinas"])))

(defn función-complement-10
  []
  (let [liss-1 (fn [lista] (list? lista))
        liss (complement liss-1)]
    (liss {:a 1 :b 2})))

(defn función-complement-11
  []
  (let [spstr-1 (fn [lista] (string? (map str lista)))
        spstr (complement spstr-1)]
    (spstr [1 2 3 "Manuel" \a])))

(defn función-complement-12
  []
  (let [isch-1 (fn [palabra] (char? (first (take 1 palabra))))
        isch (complement isch-1)]
    (isch "Manuel")))

(defn función-complement-13
  []
  (let [iske-1 (fn [lista] (keyword? (first (vals lista))))
        iske (complement iske-1)]
    (iske {:nombre "Manuel" :apellido "Salinas"})))

(defn función-complement-14
  []
  (let [isva-1 (fn [lista] (int? (first (vals lista))))
        isva (complement isva-1)]
    (isva {:numeroc 16920405 :edad 22})))

(defn función-complement-15
  []
  (let [seqfi-1 (fn [lista](seqable?  (filter string? lista)))
        seqfi (complement seqfi-1)]
    (seqfi ["Manuel" 2 3 5 \a])))

(defn función-complement-16
  []
  (let [iscol-1 (fn [lista] (coll? (flatten lista)))
        iscol (complement iscol-1)]
    (iscol {["Manuel"] ["Salinas"]})))

(defn función-complement-17
  []
  (let [iseqi-1 (fn [lista] (sequential? (map double? lista)))
        iseqi (complement iseqi-1)]
    (iseqi [1 6 5 4 33/2 3/1])))

(defn función-complement-18
  []
  (let [raci-1 (fn [lista] (ratio? (last lista)))
        raci (complement raci-1)]
    (raci [2 3 4 56 78])))

(defn función-complement-19
  []
  (let [constr-1 (fn [palabra numero] (string? (concat palabra numero)))
        constr (complement constr-1)]
    (constr ["Manuel"] [1692])))

(defn función-complement-20
  []
  (let [inn-1 (fn [lista] (indexed? lista))
        innf (complement inn-1)]
    (innf '(1 2 3 4 "Manuel"))))


(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

(defn función-constantly-1
  []
  (let [lista [1 2 3 4 5] 
        fint (constantly lista)]
   (fint ["Manuel" "Salinas"]) ))

(defn función-constantly-2
  []
  (let [argu true
        fuarg (constantly argu)]
    (fuarg [3 4 "Manuel" "Salinas"])))

(defn función-constantly-3
  []
  (let [argu int?
        fuarg (constantly (map argu [1 2 3.1 4 5]))]
    (fuarg [3 4 "Manuel" "Salinas"])))

(defn función-constantly-4
  []
  (let [argu even?
        fuarg (constantly (filter argu [1 23 4 7 9]))]
    (fuarg [11 23 4 5 "manuel"])))

(defn función-constantly-5
  []
  (let [argu [1 2 3 4 5]
        fuarg (constantly (filter even? argu))]
    (fuarg [1 2 3 4 5])))

(defn función-constantly-6 
  []
  (let [argu 3
        fuarg (constantly argu)]
    (fuarg {:a 1 :b2 2})))

(defn función-constantly-7
  []
  (let [argumento [1 4 6 2]
        funarg (constantly (map inc argumento))]
    (funarg {:a 1 :b 2 :c 3 :d 4})))

(defn función-constantly-8
  []
  (let [argu '("Manuel" "Salinas")
        funarg (constantly (peek argu))]
    (funarg 1)))

(defn función-constantly-9
  []
  (let [argu [3.4 2 3 "Manuel" "Salinas"]
        funarg (constantly (first argu))]
    (funarg \a)))

(defn función-constantly-10
  []
  (let [argu 1 
        funarg (constantly (> argu 5))]
    (funarg "Manuel")))

(defn función-constantly-11
  []
  (let [argu "Salinas"
        funarg (constantly (take 1 argu))]
    (funarg [1 4 524 5 6 4])))

(defn función-constantly-12
  []
  (let [argu {:a "ITO" :b 2}
        funarg (constantly (keys argu))]
    (funarg 1 2 3)))

(defn función-constantly-13
  []
  (let [argu {:nombre "Manuel" :apellido "Salinas"}
        funarg (constantly (vals argu))]
    (funarg [3/6 3/8 6/4])))

(defn función-constantly-14
  []
  (let [argu 3/9 
        funarg (constantly (/ 5 argu))]
    (funarg {:a 1 :b 2})))

(defn función-constantly-15
  []
  (let [argu '(\m \a \n \u)
        funarg (constantly (map str argu))]
    (funarg 12)))

(defn función-constantly-16
  []
  (let [argu [12.2 3.5 6.7 8.0]
        funarg (constantly (map dec argu))]
    (funarg "Manuel" "Salinas")))

(defn función-constantly-17 
  []
  (let [argu "1"
        funarg (constantly (Integer. argu))]
    (funarg '(5 6 7 3 24))))

(defn función-constantly-18
  []
  (let [argu 12
        funarg (constantly (str argu))]
    (funarg "Manuel" "Salinas")))

(defn función-constantly-19
  []
  (let [argu 3.4
        funarg (constantly (double? argu))]
    (funarg {:a "Manuel" :b "Salinas"})))

(defn función-constantly-20
  []
  (let [argu [1 2 35]
        funarg (constantly (concat argu "Manuel"))]
    (funarg '(2.4 5 67 44))))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [fun1 (fn [x] (map ((fn [y] (/ 7 y))x) x))
        funp (every-pred vector? fun1)]
    (funp '(1 2 3 4 5))))

(defn función-every-pred-2
  []
  (let [arg int?
        funp (every-pred arg string?)]
    (funp  "Manuel")))

(defn función-every-pred-3
  []
  (let [fun1 (fn [x] (pos? x))
        fun2 (fn [x] (number? x))
        funp (every-pred fun1 fun2)]
    (funp -1)))

(defn función-every-pred-4
  []
  (let [fun-1 (fn [x] (list? x))
        fun-2 (fn [x] (coll? x))
        funp (every-pred fun-1 fun-2)]
    (funp [1 2 3 4])))

(defn función-every-pred-5 
  []
  (let [fun-1 (fn [x] (counted? x))
        fun-2 (fn [x] (coll? (conj [] x)))
        funp (every-pred fun-1 fun-2)]
    (funp 1)))

(defn función-every-pred-6
  []
  (let [fun-1 (fn [x] (int? x))
        fun-2 (fn [x] (pos? x))
        funp (every-pred fun-1 fun-2)]
    (funp -145)
    ))

(defn función-every-pred-7
  []
  (let [fun-1 (fn [x] (decimal? x))
        fun-2 (fn [x](double? x)) 
        funp (every-pred fun-1 fun-2)]
    (funp 3.34)))

(defn función-every-pred-8
  []
  (let [fun-1 (fn [x] (pos? x))
        fun-2 (fn [x] (complement ((fn [x] (neg? x))x)))
        funp (every-pred fun-2 fun-1)]
    (funp -1)))

(defn función-every-pred-9
  []
  (let [fun-1 (fn [x] (even? x))
        fun-2 (fn [x] (number? x))
        funp (every-pred fun-1 fun-2)]
    (funp 2)))

(defn función-every-pred-10
  []
  (let [fun-1 (fn [x] (float? x))
        fun-2 (fn [x] pos? x)
        funp (every-pred fun-1 fun-2)]
    (funp -2N)))

(defn función-every-pred-11
  []
  (let [fun-1 (fn [x] (string? x))
        fun-2 (fn [x] (int? (count x)))
        funp (every-pred fun-1 fun-2)]
    (funp "Manuel")))

(defn función-every-pred-12
  []
  (let [fun-1 (fn [x] (keyword? (keys x)))
        fun-2 (fn [x] (coll? x))
        funp (every-pred fun-1 fun-2)]
    (funp {:nombre "Manuel" :apellido "Salinas"})))

(defn función-every-pred-13
  []
  (let [fun-1 (fn [x] (list? x))
        fun-2 (fn [x] (coll? (filter int? x)))
        funp (every-pred fun-1 fun-2)]
    (funp [1 2 3 4 5])))

(defn función-every-pred-14
  []
  (let [fun-1 (fn [x] (integer? x))
        fun-2 (fn [x] (some? x))
        funp (every-pred fun-1 fun-2)]
    (funp 13N)))

(defn función-every-pred-15
  []
  (let [fun-1 (fn [x] (ident? (first (keys x))))
        fun-2 (fn [x] (keyword? (first (keys x))))
        funp (every-pred fun-1 fun-2)]
    (funp {:nombre "Manuel" :apellido "Salinas"})))

(defn función-every-pred-15
  []
  (let [fun-1 (fn [x] (ident? (first (keys x))))
        fun-2 (fn [x] (keyword? (first (keys x))))
        funp (every-pred fun-1 fun-2)]
    (funp {:nombre "Manuel" :apellido "Salinas"})))

(defn función-every-pred-16
  []
  (let [fun-1 (fn [x] (char? x))
        fun-2 (fn [x] (some? x))
        funp (every-pred fun-1 fun-2)]
    (funp \m)))

(defn función-every-pred-17
  []
  (let [fun-1 (fn [x] (vector? x))
        fun-2 (fn [x] (sequential? x))
        funp (every-pred fun-1 fun-2)]
    (funp [1 2 3 4 5 6])))

(defn función-every-pred-18
  []
  (let [fun-1 (fn [x] (some? x))
        fun-2 (fn [x] (map? x))
        funp (every-pred fun-1 fun-2)]
    (funp {:nombre "Manuel" :apellido "Salinas"})))

(defn función-every-pred-19
  []
  (let [fun-1 (fn [x] (number? x))
        fun-2 (fn [x] (int? (count x)))
        funp (every-pred fun-1 fun-2)]
    (funp "Manuel")))

(defn función-every-pred-20
  []
  (let [fun-1 (fn [x] (symbol? x))
        fun-2 (fn [x] (char? x))
        funp (every-pred fun-1 fun-2)]
    (funp \a)))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14) 
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [fun1 (fn [x y] (if(< x y) (inc x) (inc y)))
        ffun (fnil fun1 2)]
    (ffun nil 8)))

(defn función-fnil-2
  []
  (let [fun1 (fn [lista numero] (get lista numero))
        ffun (fnil fun1 ["Manuel" "Salinas"])]
    (ffun nil 1)))

(defn función-fnil-3
  []
  (let [fun1 (fn [predi lista] (map predi lista))
        ffun (fnil fun1 inc )]
    (ffun nil [1 2 3 4 5 6])))

(defn función-fnil-4
  []
  (let [fun1 (fn [predi lista] (filter predi lista ))
        ffun (fnil fun1 int?)]
    (ffun nil [1 "Manuel" 4 -2 \s])))

(defn función-fnil-5
  []
  (let [fun1 (fn [x y] (= x y))
        ffun (fnil fun1 20)]
    (ffun nil 22)))

(defn función-fnil-6
  []
  (let [fun1 (fn [x y] (< x y))
        ffun (fnil fun1 10N)]
    (ffun nil 11M)))

(defn función-fnil-7
  []
  (let [fun1 (fn [x y] (x y))
        ffun (fnil fun1 int?)]
    (ffun nil 5)))

(defn función-fnil-8
  []
  (let [fun1 (fn [x y z] (count (x y z)))
        ffun (fnil fun1 filter)]
    (ffun nil string? [1 3 4 6 "Manuel" "Salinas"])))

(defn función-fnil-9
  []
  (let [fun1 (fn [x y z] (double? (x y z)))
        ffun (fnil fun1 get)]
    (ffun nil [1 24 5.4 32 5] 2)))

(defn función-fnil-10
  []
  (let [fun1 (fn [x y] (- x y))
        ffun (fnil fun1 -12)]
    (ffun nil 98)))

(defn función-fnil-11
  []
  (let [fun1 (fn [x y a b] (+ (get x a) (get y b)))
        ffun (fnil fun1 [1 23 567.3] )]
    (ffun nil [145 535 6] 1 2)))

(defn función-fnil-12
  []
  (let [fun1 (fn [x y z] (apply str (x y z)))
        ffun (fnil fun1 take)]
    (ffun nil 1 "Manuel")))

(defn función-fnil-13
  []
  (let [fun1 (fn [x y] (>= x y))
        ffun (fnil fun1 12)]
    (ffun nil 12N)))

(defn función-fnil-14
  []
  (let [fun1 (fn [x y z] (x (/ y z)))
        ffun (fnil fun1 double?)]
    (ffun nil 2 4.5)))

(defn función-fnil-15
  []
  (let [fun1 (fn [x y z] (x (y z)))
        ffun (fnil fun1 keyword?)]
    (ffun nil keys {:nombre "Manuel" :apellido "Salinas"})))

(defn función-fnil-16
  []
  (let [fun1 (fn [x z y w] (* (x z) (y w)))
        ffun (fnil fun1 first)]
    (ffun nil [2 45 6 7] last [45435 6 3 342])))

(defn función-fnil-17
  []
  (let [fun1 (fn [x y] (str (* x y)))
        ffun (fnil fun1 13)]
    (ffun nil 5657)))

(defn función-fnil-18
  []
  (let [fun1 (fn [x y] (even? (* x y)))
        ffun (fnil fun1 2335)]
    (ffun nil 43)))

(defn función-fnil-19
  []
  (let [fun1 (fn [x y] (boolean? (= x y)))
        ffun (fnil fun1 "Manuel")]
    (ffun nil "Salinas")))

(defn función-fnil-20
  []
  (let [fun1 (fn [x y] (coll? (concat x y)))
        ffun (fnil fun1 ["manuel" "salinas"])]
    (ffun nil [22 1998])))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


(defn función-juxt-1
  []
  (let [jfun-1 (fn [x] (count x))
        jfun-2 (fn [x] (first x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun [1 2 3 5 5])))

(defn función-juxt-2
  []
  (let [jfun-1 (fn [x] (first x))
        jfun-2 (fn [x] (last x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun ["Manuel"])))

(defn función-juxt-3
  []
  (let [jfun-1 (fn [x y] (= x y))
        jfun-2 (fn [x y] (> x y))
        jfun (juxt jfun-1 jfun-2)]
    (jfun 4 7)))

(defn función-juxt-4
  []
  (let [jfun-1 (fn [x y] (* x y))
        jfun-2 (fn [x y] (double? (* x y)))
        jfun (juxt jfun-1 jfun-2)]
    (jfun 567.4 434)))

(defn función-juxt-5
  []
  (let [jfun-1 (fn [x y] (/ x y))
        jfun-2 (fn [x y] (pos? (- x y)))
        jfun (juxt jfun-1 jfun-2)]
    (jfun -3453 54)))

(defn función-juxt-6
  []
  (let [jfun-1 (fn [x] (pos? x))
        jfun-2 (fn [x] (* -1 x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun 4)))

(defn función-juxt-7
  []
  (let [jfun-1 (fn [x] (neg? x))
        jfun-2 (fn [x] (pos? x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun -4)))

(defn función-juxt-8
  []
  (let [jfun-1 (fn [x y] (get x y))
        jfun-2 (fn [x y] (get x (inc y)))
        jfun (juxt jfun-1 jfun-2)]
    (jfun [123456 453 2 7 4] 2)))

(defn función-juxt-9
  []
  (let [jfun-1 (fn [x] (first x))
        jfun-2 (fn [x] (last x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun [12 "manuel" "salinas"])))

(defn función-juxt-10
  []
  (let [jfun-1 (fn [x] (map inc x))
        jfun-2 (fn [x] (filter pos? x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun [-1 3 4 -5 32 -6])))

(defn función-juxt-11
  []
  (let [jfun-1 (fn [x] (keys x))
        jfun-2 (fn [x] (vals x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun {:nombre "Manuel" :apellido "Salinas" :carrera "ISC"})))

(defn función-juxt-12
  []
  (let [jfun-1 (fn [x] (peek x))
        jfun-2 (fn [x] (+ (peek x) (peek x)))
        jfun (juxt jfun-1 jfun-2)]
    (jfun '(22 4 5 2432 6675))))

(defn función-juxt-13
  []
  (let [jfun-1 (fn [x] (count x))
        jfun-2 (fn [x] (sequential? x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun {1 2 3 4 5 3})))

(defn función-juxt-14
  []
  (let [jfun-1 (fn [x] (map dec x))
        jfun-2 (fn [x] (map inc x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun [1 2 3 423 545])))

(defn función-juxt-15
  []
  (let [jfun-1 (fn [x] (string? x))
        jfun-2 (fn [x] (Integer. x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun "1321")))

(defn función-juxt-16
  []
  (let [jfun-1 (fn [x y] (+ (Integer. x) (Integer. y)))
        jfun-2 (fn [x y] (* (Integer. x) (Integer. y)))
        jfun (juxt jfun-1 jfun-2)]
    (jfun "123" "435")))

(defn función-juxt-17
  []
  (let [jfun-1 (fn [x y] (concat x y))
        jfun-2 (fn [x y] (* (first x) (first y)))
        jfun (juxt jfun-1 jfun-2)]
    (jfun [2.4 546 4 436] [23423 453.53 435 7 453 4])))

(defn función-juxt-18
  []
  (let [jfun-1 (fn [x] (flatten x))
        jfun-2 (fn [x] (count x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun [["Manuel" ["Salinas"]] [22 [1998]]])))

(defn función-juxt-19
  []
  (let [jfun-1 (fn [x] (seqable? x))
        jfun-2 (fn [x] (seq? x))
        jfun (juxt jfun-1 jfun-2)]
    (jfun '("manuel" 213 "Salinas" \q))))

(defn función-juxt-20
  []
  (let [jfun-1 (fn [x] (number? x))
        jfun-2 (fn [x] (string? (str x)))
        jfun (juxt jfun-1 jfun-2)]
    (jfun 3242342)))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)


(defn función-partial-1
  []
  (let [funp (fn [x y] (if (< x y) (inc x) (inc y)))
        ffun (partial funp 2)]
    (ffun 8)))

(defn función-partial-2
  []
  (let [funp (fn [x y] (x y))
        ffun (partial funp int?)]
    (ffun 5)))

(defn función-partial-3
  []
  (let [funp (fn [x y] (* x y))
        pfun (partial funp 324)]
    (pfun 5.6)))

(defn función-partial-4
  []
  (let [funp (fn [x y] (> x y))
        pfun (partial funp 546)]
    (pfun 345)))

(defn función-partial-5
  []
  (let [funp (fn [x y] (int? (/ x y)))
        pfun (partial funp 534)]
    (pfun 89)))

(defn función-partial-6
  []
  (let [funp (fn [x y] (string? (str (+ x y))))
        pfun (partial funp 546)]
    (pfun 645)))

(defn función-partial-7
  []
  (let [funp (fn [x y] (Integer. (str (- x y))))
        pfun (partial funp)]
    (pfun -213 3)))

(defn función-partial-8
  []
  (let [funp (fn [x y] (vector? (concat x y)))
        pfun (partial funp [123 23 44 "Manuel"])]
    (pfun ["Salinas" 123 4324 3])))

(defn función-partial-9
  []
  (let [funp (fn [x y] (map x y))
        pfun (partial funp inc)]
    (pfun [3424 444 54.6546 564])))

(defn función-partial-10
  []
  (let [funp (fn [x y] (filter x y))
        pfun (partial funp int?)]
    (pfun ["Manuel" "Salinas" 234 -42 425 323 5.34])))

(defn función-partial-11
  []
  (let [funp (fn [x y] (string? (get x y)))
        pfun (partial funp ["Manuel" "Salinas" 213 34 2342 32])]
    (pfun 1)))

(defn función-partial-12
  []
  (let [funp (fn [x y z] (decimal? (/ x (* y z))))
        pfun (partial funp 645)]
    (pfun 234 32.4)))

(defn función-partial-13
  []
  (let [funp (fn [x y] (take x y))
        pfun (partial funp 3)]
    (pfun "Manuel")))

(defn función-partial-14
  []
  (let [funp (fn [x y] (char? (first (take x y))))
        pfun (partial funp 2)]
    (pfun "Salinas")))

(defn función-partial-15
  []
  (let [funp (fn [x y] (double? (/ x y)))
        pfun (partial funp 23434.32)]
    (pfun 4/5)))

(defn función-partial-16
  []
  (let [funp (fn [x y] (concat x y))
        pfun (partial funp ["Manuel" "Salinas"])]
    (pfun [22 1998])))

(defn función-partial-17
  []
  (let [funp (fn [x y] (x (flatten y)))
        pfun (partial funp count)]
    (pfun [["Manuel" [22]] ["Salinas" [1998]]])))

(defn función-partial-18
  []
  (let [funp (fn [x y] (apply str (concat x y)))
        pfun (partial funp [1321 23 12 2 2])]
    (pfun ["Manuel" "Salinas"])))

(defn función-partial-19
  []
  (let [funp (fn [x y] (even? (+ x y)))
        pfun (partial funp 3412)]
    (pfun 2321)))

(defn función-partial-20
  []
  (let [funp (fn [x y] (* (+ x x) (* y y)))
        pfun (partial funp 132)]
    (pfun 12434)))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-some-fn-1
  []
  (let [fun-1 (fn [x] (vector? x))
        fun-2 (fn [x] (sequential? x))
        funs (some-fn fun-1 fun-2)]
    (funs [1 2 3 4 5 6])))

(defn función-some-fn-2
  []
  (let [fun-1 (fn [x] (char? x))
        fun-2 (fn [x] (some? x))
        funs (some-fn fun-1 fun-2)]
    (funs \m)))

(defn función-some-fn-3
  []
  (let [fun-1 (fn [x] (string? x))
        fun-2 (fn [x] (int? (count x)))
        funs (some-fn fun-1 fun-2)]
    (funs "Salinas")))

(defn función-some-fn-4
  []
  (let [fun-1 (fn [x] (number? x))
        fun-2 (fn [x] (pos? x))
        funs (some-fn fun-1 fun-2)]
    (funs -3)))

(defn función-some-fn-5
  []
  (let [fun-1 (fn [x] (string? x))
        fun-2 (fn [x] (char? x))
        funs (some-fn fun-1 fun-2)]
    (funs "a")))

(defn función-some-fn-6
  []
  (let [fun-1 (fn [x] (pos? x))
        fun-2 (fn [x] (neg? x))
        funs (some-fn fun-1 fun-2)]
    (funs -345345)))

(defn función-some-fn-7
  []
  (let [fun-1 (fn [x] (vector? x))
        fun-2 (fn [x] (list? x))
        funs (some-fn fun-1 fun-2)]
    (funs {:apellido "Salinas" :nombre "Manuel"})))

(defn función-some-fn-8
  []
  (let [fun-1 (fn [x] (int? (Integer. x)))
        fun-2 (fn [x] (string? x))
        funs (some-fn fun-1 fun-2)]
    (funs "1998")))

(defn función-some-fn-9
  []
  (let [fun-1 (fn [x] (pos? (get x 0)))
        fun-2 (fn [x] (pos? (last x)))
        funs (some-fn fun-1 fun-2)]
    (funs [-123 34 454646 76])))

(defn función-some-fn-10
  []
  (let [fun-1 (fn [x] (some? (filter int? x)))
        fun-2 (fn [x] (some? (filter string? x)))
        funs (some-fn fun-1 fun-2)]
    (funs ["Manuel" "Salinas" 234 45 546 -4])))

(defn función-some-fn-11
  []
  (let [fun-1 (fn [x] (keyword? (keys x)))
        fun-2 (fn [x] (coll? x))
        funs (some-fn fun-1 fun-2)]
    (funs {:nombre "Manuel" :apellido "Salinas"})))

(defn función-some-fn-12
  []
  (let [fun-1 (fn [x] (list? x))
        fun-2 (fn [x] (coll? (filter int? x)))
        funs (some-fn fun-1 fun-2)]
    (funs [1 2 3 4 5])))

(defn función-some-fn-13
  []
  (let [fun-1 (fn [x] (seq? x))
        fun-2 (fn [x] (sequential? x))
        funs (some-fn fun-1 fun-2)]
    (funs [["Manuel" [1998]] ["Salinas"]])))

(defn función-some-fn-14
  []
  (let [fun-1 (fn [x] (integer? (* x x)))
        fun-2 (fn [x] (integer? (+ x x)))
        funs (some-fn fun-1 fun-2)]
    (funs -342)))


(defn función-some-fn-15
  []
  (let [fun-1 (fn [x] (coll? (concat [] x)))
        fun-2 (fn [x] (coll? (into [] x)))
        funs (some-fn fun-1 fun-2)]
    (funs [123 4545 435])))

(defn función-some-fn-16
  []
  (let [fun-1 (fn [x] (ratio? x))
        fun-2 (fn [x] (rational? x))
        funs (some-fn fun-1 fun-2)]
    (funs 123/7)))

(defn función-some-fn-17
  []
  (let [fun-1 (fn [x] (vector? (concat [] x)))
        fun-2 (fn [x] (vector? x))
        funs (some-fn fun-1 fun-2)]
    (funs '(123243 435 "Manuel" 34543))))

(función-partial-1)

(defn función-some-fn-18
  []
  (let [fun-1 (fn [x] (boolean? (string? x)))
        fun-2 (fn [x] (string? x))
        funs (some-fn fun-1 fun-2)]
    (funs \a)))

(defn función-some-fn-19
  []
  (let [fun-1 (fn [x] (some? x))
        fun-2 (fn [x] (map? x))
        funs (some-fn fun-1 fun-2)]
    (funs {:nombre "Manuel" :apellido "Salinas"})))

(defn función-some-fn-20
  []
  (let [fun-1 (fn [x] (number? x))
        fun-2 (fn [x] (int? (count x)))
        funs (some-fn fun-1 fun-2)]
    (funs "Manuel")))



(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)
























































































































